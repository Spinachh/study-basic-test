#-*-coding:utf-8-*-

from datetime import datetime,timedelta
import datetime
import pandas as pd
import numpy as np

df = pd.read_csv('datas.csv')
# print(df.head())
# print(df.tail())
# print(df.info())

#取出日期的这一列所有数据
time_col = df.iloc[:,0]
#取表中的所有值
time_arrs = time_col.values
# print(arrs)

#取出收盘价的这一列所有数据
close_price_col = df.iloc[:,2]
#取表中的所有值
close_price_arrs = close_price_col.values
# print(arrs)


#1.	将日期作为“键”，收盘价作为“值”存为一个字典

content_dict = dict(zip(time_arrs,close_price_arrs))
print('-----------这是第一题的运行结果-----------')
print(content_dict)

#2.	假设1月15日的收盘价为12.5，将该数据加入字典。

content_dict['2020/1/15'] = 12.5
print('-----------这是第二题的运行结果-----------')
print(content_dict)

#3.	假设现在是1月12日，使用datetime模块查询四天前的收盘价。
# >>> theday = datetime.datetime(int(2020), int(1), int(12))
# >>> delta = datetime.timedelta(days=4)
# >>> other_day = (theday - delta).strftime('%Y-%m-%d')
# >>> print(other_day)

now_time = datetime.datetime(int(2020), int(1), int(12))
tar_time = (now_time - datetime.timedelta(days=4)).strftime('%Y/%m/%d')
y_time = tar_time.split('/')[0]
m_time = tar_time.split('/')[1].replace('0','')
d_time = tar_time.split('/')[2].replace('0','')
tar_time = '/'.join([y_time,m_time,d_time])
tar_price = content_dict[tar_time]
print('-----------这是第三题的运行结果-----------')
print('-----------收盘价为：%s-----------'%tar_price)


#4.	将1月13日的收盘价修改为12.34。
content_dict['2020/1/13'] = 12.34
print('-----------这是第四题的运行结果-----------')
print(content_dict)

#5.	假设有一个交易策略，如果当期价格比前一期价格高，则买进，第二期卖出。
# 初始资产为10000元，用50%的现金买入股票，买入股票份额为整数。要求：产生一个持有股票份额的字典对象。

source_price = 10000
stock_price = 1 #股票初始价
stock_num = source_price // 2 // stock_price #购买了多少股
stock_dict = {'1':stock_num}    #持有股字典
print('-----------股票价为：%d 的时候持有股字典对象：%s-----------'%(stock_price,stock_dict))


# 6. 利用NumPy模块获取需要买进的日期及当天的股价。
with open('datas.csv',encoding='utf-8') as f:
    data = np.loadtxt(f,str,delimiter=',')
    # print(data[:2])
# print(data)
dd_time = '2020/1/8'        #买进的日期
for item in data[1:]:
    if item[0] == dd_time:
        print('-----------买进的日期股票价为：%s-----------'%(item[1]))


# 7. 利用Matplotlib模块绘制某只股票2020年1月前半个月的两种价格（开盘价和收盘价）曲线在同一图中，并添加必要的标题和坐标轴说明。
import matplotlib.pyplot as plt

x = df.iloc[:,0]
y1 = df.iloc[:,1]
y2 = df.iloc[:,2]

plt.plot(x, y1, color="r", linestyle="-", marker="^", linewidth=1)

plt.plot(x, y2, color="b", linestyle="-", marker="s", linewidth=1)

plt.xlabel("time")
plt.ylabel("price")
plt.xticks(rotation=90)
plt.show()

# 8. 写出交易策略完整的设计思路。
# 9. 写出绘图的基本流程。
    #1.导入画图的三方库
    #2.使用pandas获取到x轴(时间)，y轴的数据(开盘价和收盘价)
    #3.使用matplotlib画出y1为开盘价的曲线，y2为收盘价的曲线
    #4.设置x、y轴的标签名称
    #5.由于x轴时间显示会重叠，所以将标签旋转90度
    #6.展示图片
    
# 10. 给出程序代码以及所有功能正确运行的截图证明。

