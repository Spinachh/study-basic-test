#-*-coding:utf-8-*-
#开发个小游戏——猜年龄
# 注册

def register():
    '''注册'''
    count = 0
    while count < 3:
        name_inp = input('请输入用户名: ')
        pwd_inp = input('请输入密码: ')
        re_pwd_inp = input('再次输入以确认: ')
        if pwd_inp == re_pwd_inp:
            with open('user_info', 'a', encoding='utf-8') as fa:
                fa.write(f'{name_inp}:{pwd_inp}\n')
                print('注册成功')
                break

        else:
            print('两次密码输入不一致')
            count += 1


# 登录
def login():
    '''登录'''
    count = 0
    while count < 3:
        name_inp = input('请输入用户名: ')
        pwd_inp = input('请输入密码: ')

        with open('subject_4_user_info', 'r', encoding='utf-8') as fr:
            for i in fr:
                print(i)
                name, pwd = i.split(':')

                if name_inp == name.strip() and pwd_inp == pwd.strip():
                    print('登录成功')
                    count = 3
                    break

            else:
                print('密码或用户名错误')
                count += 1


# 猜年龄
def guess_age():
    '''猜年龄游戏'''

    age_count = 0
    age = 18

    while True:
        age_inp = input('请输入你猜的年龄:')
        if not age_inp.isdigit():
            print('输入错误')
            continue

        age_inp_int = int(age_inp)
        if age_inp_int > age:
            print('猜大了')
        elif age_inp_int < age:
            print('猜小了')
        else:
            print('猜对了')
            print('获得两次选择奖品的机会哦')
            award()
            break

        age_count += 1


# 选奖品
def award():
    '''选奖品'''
    award_dict = {
        '0': '马云',
        '1': '马化腾',
        '2': '马冬梅',
    }
    print(award_dict)

    choice_count = 0  # 计数
    choice_dic = {}

    while choice_count < 2:

        choice = input('请输入奖品编号: ')
        award = award_dict[choice]
        print(f'你选择的奖品是: {award}')

        # 保存用户选择信息
        if award in choice_dic:
            choice_dic[award_dict[choice]] += 1
        else:
            choice_dic[award_dict[choice]] = 1
        print(f'已选奖品为: {choice_dic}')
        choice_count += 1

    print(f'你的奖品为: {choice_dic}')

# 开始游戏
def play():
    '''开始游戏'''
    register()
    login()
    guess_age()


play()