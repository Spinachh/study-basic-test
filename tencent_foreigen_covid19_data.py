#-*-coding:utf-8-*-

import json
import requests
import csv
from pyecharts import Map


def get_data(url,headers):

    response = requests.get(url,headers=headers).text
    # print(type(response))

    content = json.loads(response)
    # print(type(content),content)

    data_all = content['data']
    # return data_all
    with open('rankList.csv', 'a', newline='', encoding='utf-8-sig') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(('name','continent','date','isUpdated','confirmAddCut',
                        'confirm','suspect','dead','heal','nowConfirm',
                        'confirmCompare','nowConfirmCompare','healCompare','deadCompare'))

        for item in data_all:

            # print(item)
            name = item['name']
            continent = item['continent']
            date = item['date']
            isUpdated = item['isUpdated']
            confirmAddCut = item['confirmAddCut']
            confirm = item['confirm']
            suspect = item['suspect']
            dead = item['dead']
            heal = item['heal']
            nowConfirm = item['nowConfirm']
            confirmCompare = item['confirmCompare']
            nowConfirmCompare = item['nowConfirmCompare']
            healCompare = item['healCompare']
            deadCompare = item['deadCompare']

            data_lst = [name,continent,date,isUpdated,confirmAddCut,
                        confirm,suspect,dead,heal,nowConfirm,
                        confirmCompare,nowConfirmCompare,healCompare,deadCompare]

            f_csv.writerow(data_lst)
            print("===============%s is Finshed!==============="%name)
    return data_all

def add_map(attr,value):
    map0 = Map("Covid-19-ConfirmNUm", width=1200, height=900)
    map0.add("世界地图", attr, value, maptype="world",visual_range=[0, 100000], is_visualmap=True, visual_text_color='#000')

    map0.render(path="世界地图.html")

def main():

    url = 'https://api.inews.qq.com/newsqa/v1/automation/foreign/country/ranklist'

    headers = {
        'User-Agent': 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
    }

    with open(r'E:\study\spider\Project\data_analies\registry.json','r',encoding='utf-8') as file:
        files = json.load(file)

    # print(type(files))
    file_json = files["PINYIN_MAP"]
    # print(file_json)
    data = get_data(url,headers)

    countries = [file_json[country['name']] for country in data]

    confirmnum = [num['confirm'] for num in data]
    add_map(countries,confirmnum)

if __name__ == '__main__':
    main()